# Index

<!-- vim-markdown-toc GFM -->

- [Component: Mirror Registry](#component-mirror-registry)
  - [Part Of](#part-of)
- [Index](#index)

<!-- vim-markdown-toc -->

# Architecture

The `mirror-registry.yml` playbook performs the folowing steps:
- Install and/or upgrade the mirror registry
- Login to the mirror registry and update the pull secret
- Sync the platform images and operators to the registry
- Generate the required cluster-manifests for the clusters to get updates from the mirror registry

# Requirements

Before you can use these playbooks you will need the following:
- A Git repository with SSH access, reachable from the bastion host and the
  OpenShift cluster you wish to configure.
- A bastion host to use which can communicate with both your Git repositories
  as well as your OpenShift cluster. See [A Note About Your Bastion
  Host](#a-note-about-your-bastion-host) for more details.
- Ansible 2.9 or higher

## A Note About Your Bastion Host

If you are using these playbooks to configure and sync your mirror-registry, 
the chances are high that you also used the `openshift_install.yml` and `openshift_config_gitops.yml` playbooks 
to deploy and configure your OpenShift clusters. These playbooks will use the same bastion host to configure it. 
These playbooks are intended to be used together, and as such will use the same
bastion host. See the installation instructions for requirements on how to
setup and configure your bastion host.

# Setup

## Prepare Your Environment

You will need to prepare a number of things before you can run the mirror-registry playbook:

# Components

The following components can be configured:

| Component | Description | Documentation |
|-----------|-------------|---------------|
| `disconnected` | Configure cluster manifests for disconnected environments | [Documentation](components/disconnected.md) |
| `osus` | Install and configure OpenShift Update Service for disconnected environments | [Documentation](components/osus.md) |
| `operatorhub` | Configure Operator Hub default catalog source for disconnected environments | [Documentation](components/operatorhub.md) |

# Disconnected Variables

| Option | Required/Optional | Comments | Default |
|--------|-------------------|----------|---------|
| mirror_registry_version | **Required** | The subscription channel to use | `latest` |
| mirror_registry_download_url | Optional | Default mirror-registry url, change this when you need other version | `https://mirror.openshift.com/pub/openshift-v4/clients/mirror-registry/{{ mirror_registry_version }}/mirror-registry.tar.gz` |
| mirror_registry_download_checksum | Optional | Default oc-mirror url, change this when you need other version | `https://mirror.openshift.com/pub/openshift-v4/clients/mirror-registry/{{ mirror_registry_version }}/mirror-registry.tar.gz.sha256` |
| mirror_registry_oc_plugin_url | Optional | Default oc-mirror url, change this when you need other version | `https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable-{{ install_version_major_minor }}/oc-mirror.tar.gz` |
| mirror_registry_install | Optional | When you want to install the mirror registry change to true | `false` |
| mirror_registry_download_dir | Optional | Change path where oc-mirror stores the workbenches | `/opt/openshift_files/mirror-registry` |
| mirror_registry_pull_secret | Optional | change pull-secret if you need other credentials or other registry's | `mirror_registry_pull_secret` |
| mirror_registry_additional_images: | Optional | List additional images to download and store in the mirror registry |  |
| mirror_registry_operator_catalogs | Required | List of catalogs to download and store in the mirror registry |  |
| mirror_registry_cert | **Required** | The certificate to use when you install the mirror registry | `mirror_registry_cert` |
| disconnected_install | Optional | When you want to install a disconnected cluster change to true | `false` |

### mirror-registry Configuration Example

```yaml
mirror_registry:
  hostname: localhost
  port: 8443
  directory: /data/repo
  user: init
  password: redhat
  ssl: /opt/openshift_files/certs
  namespace: openshift4
```
### mirror registry Additional Images Configuration Example

```yaml
mirror_registry_additional_images:
- docker.io/bitnami/sealed-secrets-controller:v0.22.0
```

### mirror registry Operator Catalogs Configuration Example

```yaml
mirror_registry_operator_catalogs: |
  - catalog: registry.redhat.io/redhat/redhat-operator-index:v{{ install_version | default(install_version_major_minor) }}
    full: false
    packages:
    - name: openshift-gitops-operator
      channels:
      - name: latest
    - name: odf-operator
      channels:
      - name: stable-{{ odf_storage_operator_channel | default(install_version_major_minor) }}
    - name: local-storage-operator
      channels:
      - name: stable
    - name: openshift-pipelines-operator-rh
      channels:
      - name: latest
    - name: serverless-operator
      channels:
      - name: stable
    - name: cluster-logging
      channels:
      - name: stable
      - name: stable-5.8
    - name: elasticsearch-operator
      channels:
      - name: stable
      - name: stable-5.8
    - name: kiali-ossm
      channels:
      - name: stable
    - name: jaeger-product
      channels:
      - name: stable
    - name: servicemeshoperator
      channels:
      - name: stable
    - name: rhacs-operator
      channels:
      - name: stable
    - name: node-maintenance-operator
      channels:
      - name: stable
    - name: metallb-operator
      channels:
      - name: stable
    - name: loki-operator
      channels:
      - name: stable
      - name: stable-5.8
    - name: compliance-operator
      channels:
      - name: stable
    - name: cincinnati-operator
      channels:
      - name: v1
  - catalog: registry.redhat.io/redhat/community-operator-index:v{{ install_version | default(install_version_major_minor) }}
    full: false
    packages:
    - name: group-sync-operator
      channels:
      - name: alpha
    - name: grafana-operator
      channels:
      - name: v5
```